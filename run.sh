#!/bin/bash

WORKDIR="${1:-$HOME}"

docker run -ti --rm \
	--volume="/home/$USER:/home/$USER" \
	--volume="/etc/group:/etc/group:ro" \
	--volume="/etc/passwd:/etc/passwd:ro" \
	--volume="/etc/shadow:/etc/shadow:ro" \
	--env="DISPLAY" \
	--workdir="$WORKDIR" \
	--user="$UID:$(id -g)" \
	registry.gitlab.com/tomaskadlec/container-tesseract-ocr:latest \
    /bin/bash
