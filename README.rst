container-tesseract-ocr
=======================

.. _Docker: http://docker.com/
.. _`Gitlab CI`: https://about.gitlab.com/gitlab-ci/
.. _`Debian testing`: https://wiki.debian.org/DebianTesting

The project provides a Docker_ image with `Tesseract OCR`_ installed. The 
image is based on `Debian testing`_. The image is build automatically via 
`Gitlab CI`_

