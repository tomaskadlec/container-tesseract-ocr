FROM debian:stretch
RUN sed -i 's|httpredir.debian.org|ftp.cz.debian.org|g' /etc/apt/sources.list
RUN apt-get update && apt-get install -y \
	procps \
	wget \
    tesseract-ocr tesseract-ocr-ces tesseract-ocr-eng lios yagf

CMD /bin/bash
